//
//  ChartView.swift
//  Telegram Contest App
//
//  Created by Zhenis on 4/7/19.
//

import UIKit

final class ChartView: UIView {
    
    var filteredTuples: Array<(x: Int, y: Int)> = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("asd")
    }
    
    override func draw(_ rect: CGRect) {
        let lines = filteredTuples
        let min = lines.min(by: { $0.0 < $1.0 })
        let multiplier = rect.width / CGFloat(lines.count)
        lines.enumerated().map { index, line in
            
            if index < lines.count - 2 {
                
                let firstX =  CGFloat(line.x) * multiplier - CGFloat(min!.x) * multiplier
                let secondX = CGFloat(lines[index + 1].x) * multiplier - CGFloat(min!.x) * multiplier
                
                drawLine(from: CGPoint(x: firstX, y: CGFloat(line.y)), to: CGPoint(x: secondX, y: CGFloat(lines[index + 1].y)))
                
            } else if index < lines.count - 1 {
                
                let firstX =  CGFloat(lines[index].x) * multiplier - CGFloat(min!.x) * multiplier
                let secondX = CGFloat(rect.width)
                drawLine(from: CGPoint(x: firstX, y: CGFloat(lines[index].y)), to: CGPoint(x: secondX, y: CGFloat(lines[index + 1].y)))
            }
        }
    }
    
    var freeform: UIBezierPath?
    
    func drawLine(from pointA: CGPoint, to pointB: CGPoint) {
        freeform = UIBezierPath()
        freeform?.move(to: pointA)
        freeform?.addLine(to: pointB)
        freeform?.close()
        freeform?.lineWidth = 1.0
        UIColor(red: 0.24, green: 0.76, blue: 0.25, alpha: 1.0).setStroke()
        freeform?.stroke()
        UIColor.red.setFill()
        freeform?.fill()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touched = touches.first!.location(in: self)
        
        print(touched)
        
    }
}
