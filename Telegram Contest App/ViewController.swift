//
//  ViewController.swift
//  Telegram Contest App
//
//  Created by Zhenis on 4/7/19.
//

import UIKit

class ViewController: UIViewController {

    var chartView: ChartView?
    var tuples: Array<(x: Int, y: Int)> = []
    var size: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - UIScreen.main.bounds.height / 3)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - UIScreen.main.bounds.height / 3))
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = UICol gor(red: 0.14, green: 0.18, blue: 0.24, alpha: 1.0)
        
        chartView = ChartView(frame: CGRect(x: 0, y: 0, width: size.width * 5, height: size.height))
        
        scrollView.addSubview(chartView!)
        scrollView.contentSize = CGSize(width: size.width * 5, height: size.height)
        scrollView.isScrollEnabled = false
        
        view.addSubview(scrollView)
        
        let slider = UISlider(frame: CGRect(x: 16, y: size.height + 16, width: size.width - 32, height: 64))
        slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        view.addSubview(slider)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        chartView?.addGestureRecognizer(gesture)
        
        self.view = view
    }
    
    @objc func tapGesture(_ tap: UITapGestureRecognizer) {
        print(tap.location(in: chartView!).x)
    }
    
    @objc func sliderChanged(_ slider: UISlider) {
        scrollView.contentOffset.x = scrollView.contentSize.width * CGFloat(slider.value)
    }
    
    func loadData() {
        
        guard let filePath = Bundle.main.path(forResource:"chart_data", ofType: "json"), let contentData = FileManager.default.contents(atPath: filePath), let json = try? JSONSerialization.jsonObject(with: contentData, options: []) else { fatalError() }
        
        guard let array = json as? [[String: Any]] else { print("array"); return }
        
        guard var columns = array.first?["columns"] as? [[AnyObject]] else { print("array of strings"); return }
        
        var dates: [Date]?
        
        // X coordinate
        
        let xstringIndex = columns.first?.firstIndex(where: { ($0 as? String) == "x" })
        columns[0].remove(at: xstringIndex!)
        
        if let timestamps = columns.first as? [Int] {
            dates = timestamps.map { timestamp in
                return Date(timeIntervalSince1970: TimeInterval(timestamp / 1000))
            }
        }
        
        // Y coordinate
        
        let ystringIndex = columns[1].firstIndex(where: { ($0 as? String) == "y0" })
        columns[1].remove(at: ystringIndex!)
        
        let yCoordinates = columns[1].map { $0 as? Int }.compactMap({$0})
        
        let days = dates?.map { date in
            return abs(Calendar.current.dateComponents([.day], from: Date(), to: date).day!)
            }.compactMap({$0})
        
        let tuples = zip(days!.reversed(), yCoordinates.map{ -($0 - 384)}).map { ($0, $1) }
        self.tuples = tuples
        chartView?.filteredTuples = tuples
    }

}

